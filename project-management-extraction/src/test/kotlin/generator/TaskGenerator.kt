package generator

import domain.Task

/**
 * Created by v.shipugin on 2019-10-23
 */
object TaskGenerator {

    fun createTask(
        title: String = "sample task title",
        description: String = "sample task description",
        projectTitle: String = "sample project title",
        status: String = "new",
        appraisal: Float = 0f,
        previousTask: Task? = null,
        nextTask: Task? = null,
        priority: Int = 0
    ): Task {

        return Task(
            title = title,
            description = description,
            projectTitle = projectTitle,
            status = status
        ).apply {
            this.appraisal = appraisal
            this.priority = priority
            changePreviousAndNextTasks(
                previousTask = previousTask,
                nextTask = nextTask
            )
        }
    }
}
