package domain

import generator.TaskGenerator
import org.assertj.core.api.Assertions.assertThat
import org.assertj.core.api.Assertions.catchThrowable
import org.junit.jupiter.api.Test
import org.junit.jupiter.params.ParameterizedTest
import org.junit.jupiter.params.provider.ValueSource

/**
 * Created by v.shipugin on 2019-10-23
 */
class TaskTest {

    @ParameterizedTest
    @ValueSource(floats = [-1f, -239f])
    fun `When appraisal is set out of range - throw IllegalArgumentException`(appraisal: Float) {
        val task = TaskGenerator.createTask()

        val thrown = catchThrowable { task.appraisal = appraisal }

        assertThat(thrown).hasMessageContaining("Appraisal can be in the range of 0..${Float.MAX_VALUE}")
    }

    @ParameterizedTest
    @ValueSource(floats = [0f, 1f, 5f, 10f, 1234f, 23487234687f])
    fun `When the correct appraisal is set - the appraisal is set`(appraisal: Float) {
        val task = TaskGenerator.createTask()

        task.appraisal = appraisal

        assertThat(task.appraisal).isEqualTo(appraisal)
    }

    @ParameterizedTest
    @ValueSource(ints = [-1, -239, 11, 23423])
    fun `When priority is set out of range - throw IllegalArgumentException`(priority: Int) {
        val task = TaskGenerator.createTask()

        val thrown = catchThrowable { task.priority = priority }

        assertThat(thrown).hasMessageContaining("Priority can be in the range of 0..10")
    }

    @ParameterizedTest
    @ValueSource(ints = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10])
    fun `When the correct priority is set - the priority is set`(priority: Int) {

        val task = TaskGenerator.createTask()

        task.priority = priority

        assertThat(task.priority).isEqualTo(priority)
    }

    @Test
    fun `When previous and next tasks is set without params - the previous and next tasks is not changed`() {
        val task = TaskGenerator.createTask(previousTask = null, nextTask = null)

        task.changePreviousAndNextTasks()

        assertThat(task.nextTask).isNull()
        assertThat(task.previousTask).isNull()
    }

}
