package domain

/**
 * Created by v.shipugin on 22/10/2019
 */
data class Task(
    val title: String,
    val description: String,
    val projectTitle: String,
    val status: String
) {
    var appraisal: Float = 0f
        set(value) {
            require(value >= 0) { "Appraisal can be in the range of 0..${Float.MAX_VALUE}" }

            field = value
        }

    var priority: Int = 0
        set(value) {
            require((0..10).contains(value)) { "Priority can be in the range of 0..10" }

            field = value
        }

    var previousTask: Task? = null
        private set

    var nextTask: Task? = null
        private set

    fun changePreviousAndNextTasks(
        previousTask: Task? = this.previousTask,
        nextTask: Task? = this.nextTask
    ) {
        this.previousTask = previousTask
        this.nextTask = nextTask
    }

}
