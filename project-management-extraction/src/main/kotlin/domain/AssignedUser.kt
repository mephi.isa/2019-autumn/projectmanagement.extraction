package domain

/**
 * Created by v.shipugin on 06/11/2019
 */
data class AssignedUser(
    val fullName: String,
    var isAuthorized: Boolean = false,
    var tasks: List<Task> = emptyList()
)
