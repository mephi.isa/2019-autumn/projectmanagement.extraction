import domain.AssignedUser
import domain.Task

/**
 * Created by v.shipugin on 22/10/2019
 */
class App {

    companion object {
        @JvmStatic
        fun main(args: Array<String>) {
            val task1 = Task(
                title = "sample task1 title",
                description = "sample task1 description",
                projectTitle = "sample project title",
                status = "NEW"
            )

            val task2 = Task(
                title = "sample task2 title",
                description = "sample task2 description",
                projectTitle = "sample project title",
                status = "NEW"
            ).also {
                it.changePreviousAndNextTasks(nextTask = task1)
                task1.changePreviousAndNextTasks(nextTask = it)
            }

            val user = AssignedUser(
                fullName = "FIO",
                isAuthorized = true,
                tasks = listOf(task1, task2)
            )

            print(user)
        }
    }

}
